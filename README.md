
![](preview.png)

# Sweetr SDDM theme

This theme has been extracted and forked from https://github.com/EliverLara/Sweet.
Thanks to [https://github.com/EliverLara](EliverLara) for this beautifull theme.


## Installation

Clone this repo into sddm themes folder:
`git clone https://gitlab.com/opeshm/sweeter-sddm-theme.git /usr/share/sddm/themes/sweetter`

